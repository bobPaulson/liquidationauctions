package main

import (
	"strings"
	"os"
	errH "repo/errorHandle"
	"net/http"
	"golang.org/x/net/html"
	"fmt"
	"io/ioutil"
	"encoding/json"
)

var (
	apiurl = "https://m.liquidation.com/api/v1/auction_view/"
	siteURL = "https://www.liquidation.com/auction/search?query=3Tk2jDeA0yrp0oe5LOro0or4TyO4uTMg95r.ITOpI-jY3G5g9teAJ1h.RDeA0yrp0oe5LOro0or4TyO4uTMg95r.ITOpI-jY3G5g9teAz50.RDeA0yrp0oe5LOro0or4TyO4uTMg95r.ITOpI-jY3G5g9teAJ1h.jDeA0yrp0oe5LOro0or4TyO4uTMg95r.ITOpI-jY3G5g9teAz50.zhJAzTCaTQeg3TmUL-jALtmg9JrY9-O69ybA9G5c3-5DuTjM9ybiKOeg3TmUL-jALtmg9JrC9-O69ybAHG5c3-5DuTjM9ybiKOeg3TmUL-jALtmg9JrC9-O69ybAHG5c3-5DuTjM9ybikfCA0sCt2Nka2Oriuya2k2CA0Qmh2Ge.9Qe5TQjM9-R2Tyj49f2ckN2ae2CA3QmD9zCC9tD2TQ3xL222TyOiua2c22VV&flag=new&sort=close_time&ascending=1&_per_page=30&_page=1"

	siteBase = "http://www.liquidation.com/auction/view?id="
	Base = "https://www.liquidation.com"
)

type Item struct {
	ProdName	string
	Price		int
	Quantity	int
}

// Note: "NextPageURI" and "PrevPageURI" are strings but will be 'nil' if empty.
type Auctions struct {
	Result []struct {
		AuctionView struct {
			ID  string `json:"id"`
			URI string `json:"uri"`
		} `json:"auction_view"`
	} `json:"result"`
	Error []struct {
		ErrorCode  string `json:"error_code"`
		ErrorText  string `json:"error_text"`
		StatusCode string `json:"status_code"`
		ErrorID    string `json:"error_id"`
	} `json:"error"`
	Meta struct {
		Time int `json:"time"`
	} `json:"meta"`
	PageNum     string      `json:"page"`
	NextPageURI interface{} `json:"next_page_uri"`
	PerPage     int         `json:"per_page"`
	More        bool        `json:"more"`
	PrevPageURI interface{} `json:"prev_page_uri"`
}


func main() {
	//l, err := GetAuctionList()
	l, err := GetAuctionFromAllPages()
	if err != nil {
		errH.LogError(err)
		return
	}
	imap, err := ItemsMap(l)
	if err != nil {
		errH.LogError(err)
		return
	}
/*
	for k, v := range imap {
		fmt.Println(k)
		for _, vv := range v {
			fmt.Println("  ", vv.ProdName)
		}
	}
*/
	for k, v := range imap {
		//fmt.Println(k)
		for _, vv := range v {
			for _, searchWd := range os.Args {
				if strings.Contains(strings.ToLower(vv.ProdName), strings.ToLower(searchWd)) {
					fmt.Println(k, ":::", vv.ProdName)
				}
			}
			//fmt.Println("  ", vv.ProdName)
		}
	}
}

func GetAuctionList() ([]string, error) {
	var auctionList []string

	resp, err := http.Get(apiurl)
	if err != nil {
		errH.LogError(err)
		return auctionList, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errH.LogError(err)
		return auctionList, err
	}

	// Unmarshal JSON.
	var f Auctions
	err = json.Unmarshal(body, &f)
	if err != nil {
		errH.LogError(err)
		return auctionList, err
	}
	if f.Error != nil && len(f.Error) > 0 {
		for _, eachErr := range f.Error {
			err = fmt.Errorf("%s", eachErr.ErrorText)
			errH.LogError(err)
		}
		return auctionList, err
	}

	for _, auc := range f.Result {
		auctionList = append(auctionList, auc.AuctionView.ID)
	}
	return auctionList, err
}

/*
type Items struct {
	ProdName	string
	Price		int
	Quantity
}
*/
func ItemsMap(auctionList []string) (map[string][]Item, error) {
	itemMap := make(map[string][]Item)
	uniqueIdList := make(map[string]struct{})
	// Create unique list
	for _, auc := range auctionList {
		id := auc[len(siteBase):]
		uniqueIdList[id] = struct{}{}
	}

	for id, _ := range uniqueIdList {
		resItem, err := GetItemsData(id)
		if err != nil {
			errH.LogError(err)
			return itemMap, err
		}
		itemMap[id] = resItem
	}
	return itemMap, nil
}

func GetItemsData(id string) ([]Item, error) {
	var items []Item
	url := fmt.Sprintf("https://www.liquidation.com/aucimg/%s/m%s.html", id[:5], id)
	//fmt.Println(url)

	resp, err := http.Get(url)
	if err != nil {
		errH.LogError(err)
		return items, err
	}
	defer resp.Body.Close()

    doc, err := html.Parse(resp.Body)
    if err != nil {
		errH.LogError(err)
		return items, err
    }

	// Find script tags and extract contents for parsing
	var f func(*html.Node, bool)
	f = func(n *html.Node, printText bool) {
		if n == nil {
			// Empty node.
			return
		}
		// Get next page
		style := "DUMMYVALUE"
		if n.Type == html.ElementNode && n.Data == "td" {
			for _, t := range n.Attr {
				if t.Key == "style" {
					style = t.Val
				}
			}
		}
		if printText && n.Type == html.TextNode && len(n.Data) > 1 {
			//fmt.Println("  ", n.Data)
			items = append(items, Item{n.Data,0,0})
			printText = false
		}
		printText = printText || (n.Type == html.ElementNode && n.Data == "td" && len(style) <= 0)
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c, printText)
		}
	}
    f(doc, false)
	return items, err
}

func GetAuctionFromAllPages() ([]string, error) {
	var totalAuctionList []string
	var auctionList []string
	var err error
	var url = siteURL

	for len(url) > 0 {
		auctionList, url, err = GetAuctionFromPage(url)
		if err != nil {
			errH.LogError(err)
			return totalAuctionList, err
		}
		totalAuctionList = append(totalAuctionList, auctionList...)
	}
	return totalAuctionList, err
}

func GetAuctionFromPage(url string) ([]string, string, error) {
	var auctionList []string
	var nextPage string
	Param := "/auction/search?query="

	resp, err := http.Get(url)
	if err != nil {
		errH.LogError(err)
		return auctionList, nextPage, err
	}
	defer resp.Body.Close()

    doc, err := html.Parse(resp.Body)
    if err != nil {
		errH.LogError(err)
		return auctionList, nextPage, err
    }

	// Find script tags and extract contents for parsing
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n == nil {
			// Empty node.
			return
		}
		// Get next page
		if n.Type == html.ElementNode && n.Data == "link" {
			rel := ""
			href := ""
			for _, t := range n.Attr {
				if t.Key == "href" {
					href = t.Val
				}
				if t.Key == "rel" {
					rel = t.Val
				}
			}
			if rel == "next" && len(href) > 50 && href[:len(Param)] == Param {
				nextPage = Base + href
			}
		}
		// Get auction link
		if n.Type == html.ElementNode && n.Data == "a" {
			aucEntry := ""
			class := ""
			for _, t := range n.Attr {
				if t.Key == "href" {
					aucEntry = t.Val
				}
				if t.Key == "class" {
					class = t.Val
				}
			}
			if len(aucEntry) > len(siteBase) && aucEntry[:len(siteBase)] == siteBase && class == "desc" {
				auctionList = append(auctionList, aucEntry)
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
    f(doc)
	return auctionList, nextPage, nil
}
